import { Injectable } from '@angular/core';
import { from, Subscription } from 'rxjs';
@Injectable({ providedIn: 'root' })
export class CommonService {
    private _rule: any;
    private readonly colors = [
        "#42A5F5",
        "#FFA726",
        "#26C6DA",
        "#66BB6A",
        "#7E57C2"]
    private w: any;
    private api: any;
    private _DBURL = '';
    private readonly error = 'API not Found';
    constructor() {
        this.w = window;
        this.api = this.w['API'];
        this.resetRule();
    }

    get DBURL() {
        return this._DBURL;
    }

    set DBURL(val: string) {
        this._DBURL = val;
    }

    get rule() {
        return this._rule;
    }

    set rule(val: any) {
        this._rule = val;
    }

    getColor(index: number) {
      return this.colors[index%(this.colors.length)];
    }

    resetRule() {
        this._rule = {
            name: '',
            desc: '',
            timestamp: Date.now(),
            collection: '',
            filters: [{
                key: '',
                label: ''
            }],
            content: [
            ]
        };
    }

    ping() {
        return this.getObservable(this.api?.ping());
    }

    connect(val: string) {
        return this.getObservable(this.api?.connect(val))
    }

    disconnect() {
        return this.getObservable(this.api?.disconnect())
    }

    getData(collection: string, schema: any, query: any, skip?: number, limit?: number) {
        return this.getObservable(this.api?.getData(collection, schema, query, skip, limit))
    }

    getTotal(collection: string, schema: any, query: any, field: string) {
        return this.getObservable(this.api?.getTotal(collection, schema, query, field))
    }

    getGroup(collection: string, schema: any, query: any, group: string, accu: string) {
        return this.getObservable(this.api?.getGroup(collection, schema, query, group, accu))
    }

    getDistinctValue(collection: string, field: string) {
        return this.getObservable(this.api?.getDistinctValue(collection, field))
    }

    getFields(collection: string) {
        return this.getObservable(this.api?.getFields(collection));
    }

    getCollection() {
        return this.getObservable(this.api?.getCollections())
    }


    favoriteConnections(val: any) {
        return this.getObservable(this.api?.favoriteConnections(val));
    }

    recentConnections(val: any) {
        return this.getObservable(this.api?.recentConnections(val))
    }

    getConnections() {
        return this.getObservable(this.api?.getConnections())
    }

    addDashboard(val: any) {
        return this.getObservable(this.api?.addDashboard(val))
    }

    deleteDashboard(val: string) {
        return this.getObservable(this.api?.deleteDashboard(val))
    }

    editDashboard(val: any) {
        return this.getObservable(this.api?.editDashboard(val))
    }

    getDashboard() {
        return this.getObservable(this.api?.getDashboard())
    }

    getObservable(promise: Promise<any>) {
        if (promise) {
            return from(promise);
        }
        // return from(Promise.resolve())
        return from(Promise.reject(this.error))
    }

    unsubscribeSubs(subs: Subscription[]) {
        subs.forEach(s => {
            s.unsubscribe();
        })
    }

}