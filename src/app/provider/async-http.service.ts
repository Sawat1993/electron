import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap, finalize } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class AsyncHttpService {

  constructor(private httpClient: HttpClient) { }

  get(url: string, options?: Object): Observable<any> {
    return this.httpClient.get(url, options);
  }

  post(url: string, data: Object, options?: Object): Observable<any> {
    return this.httpClient.post(url, data, options);
  }

  put(url: string, data: Object, options?: Object): Observable<any> {
    return this.httpClient.put(url, data, options);
  }

  delete(url: string, options?: Object): Observable<any> {
    return this.httpClient.delete(url, options);
  }

}
