import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { CommonService } from './common.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
    constructor(public router: Router, private readonly commonService: CommonService) { }
    canActivate(): boolean {
        // return true;
        if (this.commonService.DBURL) {
            return true;
        }
        this.router.navigate(['login']);
        return false;
    }
}