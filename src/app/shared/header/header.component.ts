import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/provider/common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  subs: Subscription[] = [];

  constructor(private readonly router: Router,
    private readonly commonService: CommonService) { }

  ngOnInit() {
  }

  logOut() {
    const sub = this.commonService.disconnect().subscribe();
    this.subs.push(sub);
    this.router.navigateByUrl('login');
  }

  ngOnDestroy(): void {
    this.commonService.unsubscribeSubs(this.subs);
  }

}
