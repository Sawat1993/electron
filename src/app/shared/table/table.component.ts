import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/provider/common.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnChanges, OnDestroy {

  @Input() column: {key: string, label:string}[] = [];
  @Input() collection: string = '';
  @Input() query: any = {};
  @Input() schema: any = {};
  data: any = [];
  loading: boolean = false;
  totalRecords = 0;
  limit: number = 10;
  skip: number = 0;
  subs: Subscription[] = [];

  constructor(private readonly commonService: CommonService) {}

  ngOnChanges(): void {
    this.filterChanged();
  }

  filterChanged() {
    this.loading = true;
    const sub = this.commonService.getData(this.collection, this.schema, this.query, this.skip, this.limit).subscribe(d => {
      this.data = d.data;
      this.loading = false;
      this.totalRecords = d.totalRecords
    }, e => {
      console.log(e)
      this.loading = false;
    })
    this.subs.push(sub);
  }

  loadData(event: LazyLoadEvent) {
    this.limit = event.rows || 0;
    this.skip = event.first || 0;
    this.filterChanged();
  }

  ngOnDestroy(): void {
    this.commonService.unsubscribeSubs(this.subs);
  }
}
