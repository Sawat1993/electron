import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/provider/common.service';
@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnChanges, OnDestroy {

  @Input() title = '';
  @Input() key = '';
  @Input() collection: string = '';
  @Input() query: any = {};
  @Input() schema: any = {};
  value = '';
  loading: boolean = false;
  subs: Subscription[] = [];

  constructor(private readonly commonService: CommonService) {}

  ngOnChanges(): void {
    this.filterChanged();
  }

  filterChanged() {
    if(this.key.length > 0) {
      this.loading = true;
      const sub = this.commonService.getTotal(this.collection, this.schema, this.query, this.key).subscribe(d => {
        this.value = Intl.NumberFormat().format(d);
        this.loading = false;
      }, e => {
        console.log(e)
        this.loading = false;
      })
      this.subs.push(sub);
    }
  }

  ngOnDestroy(): void {
    this.commonService.unsubscribeSubs(this.subs);
  }
}
