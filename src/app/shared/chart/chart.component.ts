
import { Component, Input, OnChanges, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/provider/common.service';
@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnChanges, OnDestroy {

  @Input() title = '';
  @Input() type = '';
  @Input() group = '';
  @Input() data: any = {}
  @Input() collection: string = '';
  @Input() query: any = {};
  @Input() schema: any = {};
  loading: boolean = false;
  subs: Subscription[] = [];
  value: any = {};

  constructor(private readonly commonService: CommonService) { }

  ngOnChanges(): void {
    this.filterChanged();
  }

  filterChanged() {
    this.loading = true;
    this.value = {datasets:[]};
    this.data.forEach((element: any, i: number) => {
      const sub = this.commonService.getGroup(this.collection, this.schema, this.query, this.group, element.accu).subscribe(d => {
        const val: any = JSON.parse(JSON.stringify(this.value));
        this.sort(d);
        val.labels = d.map((i:any) => i._id);
        val.datasets.push({label: element.accu,
          data: d.map((i:any) => i.sum),
          borderColor: this.commonService.getColor(i),
          backgroundColor: this.commonService.getColor(i)})
        this.loading = false;
        this.value = val
      }, e => {
        console.log(e)
        this.loading = false;
      })
      this.subs.push(sub);
    });

  }

  sort(items: any = []) {
    items.sort((a: any, b: any) => {
      const nameA = a._id;
      const nameB = b._id;
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });
  }

  ngOnDestroy(): void {
    this.commonService.unsubscribeSubs(this.subs);
  }
}
