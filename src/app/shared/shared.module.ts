import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TableComponent } from './table/table.component';
import { TableModule } from 'primeng/table';
import { CardComponent } from './card/card.component';
import { CardModule } from 'primeng/card';
import {ChartModule} from 'primeng/chart';
import { ChartComponent } from './chart/chart.component';


@NgModule({
  imports: [
    CommonModule,
    TableModule,
    CardModule,
    ChartModule
  ],
  declarations: [HeaderComponent, FooterComponent, TableComponent, CardComponent, ChartComponent],
  exports: [HeaderComponent, FooterComponent, TableComponent, CardComponent, ChartComponent]
})
export class SharedModule { }
