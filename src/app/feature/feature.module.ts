import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { CardModule } from 'primeng/card';
import { InputTextModule } from 'primeng/inputtext';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { RuleComponent } from './rule/rule.component';
import { AccordionModule } from 'primeng/accordion';
import { DropdownModule } from 'primeng/dropdown';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CheckboxModule } from 'primeng/checkbox';


@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    SharedModule,
    CardModule,
    InputTextModule,
    FormsModule,
    ButtonModule,
    AccordionModule,
    DropdownModule,
    CheckboxModule
  ],
  declarations: [LoginComponent, HomeComponent, RuleComponent, DashboardComponent],
  exports: [LoginComponent, HomeComponent, RuleComponent]
})
export class FeatureModule { }
