import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/provider/common.service';

@Component({
  selector: 'app-rule',
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.css']
})
export class RuleComponent implements OnInit, OnDestroy {
  rule: any;
  collections: string[] = [];
  type = {
    code: 'table',
    label: 'Table'
  };
  width = 'col-12';
  title = '';
  types = [{
    code: 'card',
    label: 'Card'
  },{
    code: 'table',
    label: 'Table'
  }, {
    code: 'line',
    label: 'Line'
  }, {
    code: 'bar',
    label: 'Bar'
  }, {
    code: 'pie',
    label: 'Pie'
  }]
  mongoFields: string[] = [];
  subs: Subscription[] = [];


  constructor(
    private readonly commonService: CommonService,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
    this.rule = this.commonService.rule;
    this.fetchMongoFields();
    const sub = this.commonService.getCollection().subscribe(d => {
      this.collections = d;
    }, e => {
      console.log(e)
    })
    this.subs.push(sub);
  }

  fetchMongoFields() {
    const sub = this.commonService.getFields(this.rule.collection).subscribe(d => {
      this.mongoFields = d;
    }, e => {
      console.log(e)
    });
    this.subs.push(sub);
  }

  addFilter() {
    this.rule.filters.push({
      key: '',
      label: ''
    })
  }

  removeFilter(i: number) {
    this.rule.filters.splice(i, 1);
  }

  addContent() {
    if(this.type.code === 'table') {
      this.rule.content.push({
        type: this.type,
        width: this.width,
        title: this.title,
        data: [{
          key: '',
          label: ''
        }]
      })
    } else if(this.type.code === 'card') {
      this.rule.content.push({
        type: this.type,
        width: this.width,
        title: this.title,
        key: ''
      })
    } else {
        this.rule.content.push({
          type: this.type,
          width: this.width,
          title: this.title,
          group: '',
          data: [{
            accu: ''
          }]
        })
    }
  }

  addData(data: any) {
    data.push({
      accu: ''
    })
  }

  addChartData(data: any) {
    data.push({

    })
  }

  removeContent(i: number) {
    this.rule.content.splice(i, 1);
  }

  removeData(i: number, data: any) {
    data.splice(i, 1);
  }

  back() {
    this.router.navigateByUrl('home')
  }

  save() {
    const sub = this.commonService.editDashboard(this.rule).subscribe(d => {
      this.router.navigateByUrl('home');
    }, e => {
      console.log(e)
    })
    this.subs.push(sub);
  }

  ngOnDestroy(): void {
    this.commonService.unsubscribeSubs(this.subs);
  }

}
