import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonService } from '../../provider/common.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  menu: any = [{
    label: 'New Connection',
    icon: 'pi-plus',
    options: [],
    highlighted: true
  }, {
    label: 'Favorites',
    icon: 'pi-star',
    options: []
  }, {
    label: 'Recents',
    icon: 'pi-clock',
    options: []
  }]
  DBURL = '';
  label = '';
  loading = false;
  errorMsg = '';
  favSelected = false;
  subs: Subscription[] = []

  constructor(private readonly commonService: CommonService,
    private readonly router: Router) { }

  ngOnInit() {
    const sub = this.commonService.getConnections().subscribe(d => {
      this.menu[1].options = d.favorites
      this.menu[2].options = d.recents
    })
    this.subs.push(sub);
  }

  loadDB() {
    this.loading = true;
    const sub = this.commonService.connect(this.DBURL).subscribe(d => {
      this.loading = false;
      this.commonService.DBURL = this.DBURL;
      this.router.navigateByUrl('home');
    }, e => {
      this.loading = false;
      this.errorMsg = e;
      setTimeout(() => {
        this.errorMsg = '';
      }, 3000);
    })

    let options = this.menu[2].options;
    options = options.filter((o: any) => o.label !== this.label || o.value !== this.DBURL);
    options.unshift({ label: this.label, value: this.DBURL });
    const recentSub = this.commonService.recentConnections(options).subscribe();

    this.subs.push(sub);
    this.subs.push(recentSub);
  }

  addToFav() {
    const options = this.menu[1].options;
    for (const option of options) {
      if (option.label === this.label && option.value === this.DBURL) {
        return false;
      }
    }
    options.unshift({ label: this.label, value: this.DBURL });
    const favSub = this.commonService.favoriteConnections(options).subscribe();
    this.subs.push(favSub);
    return true
  }

  evalFavSelected() {
    const index = this.menu[1].options.findIndex((o: any) => o.label === this.label && o.value === this.DBURL);
    if (index > -1) {
      this.favSelected = true;
    } else {
      this.favSelected = false;
    }
  }

  removeFav() {
    const index = this.menu[1].options.findIndex((o: any) => o.label === this.label && o.value === this.DBURL);
    if (index > -1) {
      this.menu[1].options.splice(index, 1)
    }
    const sub = this.commonService.favoriteConnections(this.menu[1].options).subscribe();
    this.subs.push(sub);
  }

  dbSelected(option: any) {
    this.menu.forEach((item: any) => {
      item.highlighted = false;
      item.options.forEach((option: any) => {
        option.highlighted = false;
      });
    });
    option.highlighted = true;
    this.DBURL = option.value || '';
    this.label = option.label || '';
    this.evalFavSelected();
  }

  ngOnDestroy(): void {
    this.commonService.unsubscribeSubs(this.subs);
  }

}
