import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/provider/common.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  rules: any;
  subs: Subscription[] =[];

  constructor(private readonly router: Router, private readonly commonService: CommonService) { }

  ngOnInit() {
    const sub = this.commonService.getDashboard().subscribe(d => {
      this.rules = d;
    }, e => {
      console.log(e)
    });
    this.subs.push(sub);
  }

  edit(rule: any) {
    this.commonService.rule = rule;
    this.router.navigateByUrl('rule');
  }

  viewDashboard(rule: any) {
    this.commonService.rule = rule;
    this.router.navigateByUrl('dashboard');
  }

  delete(rule: any) {
    const sub = this.commonService.deleteDashboard(rule.timestamp).subscribe(d => {
      this.rules = this.rules.filter((r: any) => r.timestamp !== rule.timestamp)
    }, e => {
      console.log(e)
    })
    this.subs.push(sub)
  }

  add() {
    this.commonService.resetRule();
    this.router.navigateByUrl('rule');
  }

  ngOnDestroy(): void {
    this.commonService.unsubscribeSubs(this.subs)
  }

}
