import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/provider/common.service';
import { LazyLoadEvent } from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  rule: any;
  selectedValues: any = {};
  subs: Subscription[] = []
  query: any = {};
  schema: any = {};
  constructor(private readonly commonService: CommonService,
    private readonly router: Router) { }

  ngOnInit(): void {
    this.rule = this.commonService.rule;

    this.rule.filters.forEach((filter: any) => {
      this.selectedValues[filter.key] = [];
      const sub = this.commonService.getDistinctValue(this.rule.collection, filter.key).subscribe(d => {
        filter.data = d;
      }, e => {
        console.log(e);
      })
      this.subs.push(sub);
    });

  }

  updateQuery() {
    const query: any = { $and: [] };
    for (const key of Object.keys(this.selectedValues)) {
      if (this.selectedValues[key]?.length > 0) {
        query.$and.push({ [key]: { $in: this.selectedValues[key] } });
      }
    }
    this.query = query.$and.length === 0 ? {} : query;
    this.updateSchema();
  }

  updateSchema() {
    const schema: any = {};
    for (const key of Object.keys(this.selectedValues)) {
      if (this.selectedValues[key]?.length > 0) {
        if (typeof this.selectedValues[key][0] === 'string' || this.selectedValues[key][0] instanceof String) {
          schema[key] = 'String';
        } else {
          schema[key] = 'Number';
        }
      }
    }
    this.schema = schema;
  }

  back() {
    this.router.navigateByUrl('home');
  }

  ngOnDestroy(): void {
    this.commonService.unsubscribeSubs(this.subs);
  }

}
