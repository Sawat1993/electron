import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  template: `
    <app-header></app-header>
    <div class="m-4"><router-outlet></router-outlet></div>
    <app-footer></app-footer>
    `
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
