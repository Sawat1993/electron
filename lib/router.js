const connection = require('./controller/connection');
const dashboard = require('./controller/dashboard');
const genericData = require('./controller/genericData');



module.exports.connect = async (e, args) => {
    return await connection.connect(args[0]);
}

module.exports.disconnect = async () => {
    return await connection.disconnect();
}

module.exports.favoriteConnections = (e, args) => {
    return connection.favoriteConnections(args[0])
}

module.exports.recentConnections = (e, args) => {
    return connection.recentConnections(args[0]);
}

module.exports.getConnections = () => {
    return connection.getConnections();
}

module.exports.getCollections = async () => {
    return await genericData.getCollections();
}

module.exports.getData = async (e, args) => {
    return await genericData.getData(args[0], args[1], args[2], args[3], args[4]);
}

module.exports.getTotal = async (e, args) => {
    return await genericData.getTotal(args[0], args[1], args[2], args[3]);
}

module.exports.getGroup = async (e, args) => {
    return await genericData.getGroup(args[0], args[1], args[2], args[3], args[4]);
}

module.exports.getDistinctValue = async (e, args) => {
    return await genericData.getDistinctValue(args[0], args[1]);
}

module.exports.getFields = async (e, args) => {
    return await genericData.getFields(args[0])
}

module.exports.addDashboard = (e, args) => {
    return dashboard.addDashboard(args[0])
}

module.exports.getDashboard = () => {
    return dashboard.getDashboard();
}

module.exports.deleteDashboard = (e, args) => {
    return dashboard.deleteDashboard(args[0]);
}

module.exports.editDashboard = (e, args) => {
    return dashboard.editDashboard(args[0]);
}
