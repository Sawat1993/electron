const database = require('../database');
const mongoose = require('mongoose');

module.exports.getCollections = async () => {
    const names = await mongoose.connection.db.listCollections().toArray();
    return names.map(n => n.name);
}

module.exports.getData = async (collection, schema, query, skip, limit) => {
    const Model = database.getModelWithSchema(collection, schema)
    const totalRecords = await Model.find(query).count();
    const data = await Model.find(query).skip(skip).limit(limit);
    return {
        totalRecords,
        data: data.map(d => d._doc)
    };
}

module.exports.getTotal = async (collection, schema, query, field) => {
    const Model = database.getModelWithSchema(collection, schema)
    const data = await Model.aggregate([
        { $match: query },
        { $group: {
            _id: null,
                sum : {
                    $sum: {
                        $toInt: "$" + field
    
                    }
            }
        } }
    ]);
    return data[0]?.sum;
}

module.exports.getGroup = async (collection, schema, query, group, accu) => {
    const Model = database.getModelWithSchema(collection, schema)
    const data = await Model.aggregate([
        { $match: query },
        { $group: {
            _id: "$" + group,
                sum : {
                    $sum: {
                        $toInt: accu ? "$" + accu : 1
    
                    }
            }
        } }
    ]);
    return data;
}

module.exports.getDistinctValue = async (collection, field) => {
    const Model = database.getModel(collection)
    const data = await Model.find().distinct(field);
    return data;
}

module.exports.getFields = async (collection) => {
    const Model = database.getModel(collection)
    const data = await Model.findOne({});
    return Object.keys(data?._doc || {});
}
