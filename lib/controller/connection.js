const database = require('../database');
const store = require('../store');
const CONSTANTS = require('../constant');

module.exports.connect = async (dburl) => {
    await database.connect(dburl);
    return 'success';
}

module.exports.disconnect = async () => {
    await database.disconnect();
    return 'success';
}

module.exports.favoriteConnections = (dburls) => {
    store.save(CONSTANTS.favorites, dburls);
    return 'success';
}

module.exports.recentConnections = (dburls) => {
    store.save(CONSTANTS.recent, dburls);
    return 'success';
}

module.exports.getConnections = () => {
    return {
        favorites: store.find(CONSTANTS.favorites) || [],
        recents: store.find(CONSTANTS.recent) || []
    }
}