const store = require('../store');
const CONSTANTS = require('../constant');

module.exports.addDashboard = (dash) => {
    const dashboard = store.find(CONSTANTS.dashboard) || [];
    dashboard.push(dash)
    store.save(CONSTANTS.dashboard, dashboard);
    return 'success';
}

module.exports.getDashboard = () => {
    return store.find(CONSTANTS.dashboard) || [];
}

module.exports.deleteDashboard = (dash) => {
    const dashboard = store.find(CONSTANTS.dashboard) || [];
    store.save(CONSTANTS.dashboard, dashboard.filter(d => d.timestamp !== dash));
    return 'success';
}

module.exports.editDashboard = (dash) => {
    let dashboard = store.find(CONSTANTS.dashboard) || [];
    dashboard = dashboard.filter(d => d.timestamp !== dash.timestamp);
    dashboard.unshift(dash);
    store.save(CONSTANTS.dashboard, dashboard);
    return 'success';
}