const Store = require('electron-store');

const store = new Store();

module.exports.save = (key, val) => {
    store.set(key, val)
}

module.exports.find = (key) => store.get(key);

module.exports.delete = (key) => {
    store.delete(key)
}