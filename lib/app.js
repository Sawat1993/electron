const { app, BrowserWindow, ipcMain } = require('electron');
const router = require('./router');
const url = require("url");
const path = require("path");
const APIS = [
  'connect',
  'disconnect',
  'getData',
  'getTotal',
  'getGroup',
  'getDistinctValue',
  'getFields',
  'getCollections',
  'favoriteConnections',
  'recentConnections',
  'getConnections',
  'addDashboard',
  'deleteDashboard',
  'editDashboard',
  'getDashboard'
];

let mainWindow

function createWindow() {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, '../lib/preload.js'),
    }
  });

  ipcMain.removeHandler('ping');
  ipcMain.handle('ping', () => 'pong');

  APIS.forEach(api => {
    ipcMain.removeHandler(api);
    ipcMain.handle(api, router[api]);
  });

  mainWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, `../dist/dashi/index.html`),
      protocol: "file:",
      slashes: true
    })
  );
  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  mainWindow.on('closed', function () {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})