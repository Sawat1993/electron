const mongoose = require('mongoose');

module.exports.connect = async (val) => {
    try {
        await mongoose.disconnect();
    } catch (error) {
        console.log(error)
    }
    await mongoose.connect(val);
    return 'success';
};

module.exports.disconnect = async () => {
    return await mongoose.disconnect();
};

module.exports.getModel = (collection) => {
    if (mongoose.models[collection]) {
        return mongoose.models[collection];
    } else {
        const Schema = new mongoose.Schema({ name: String });
        return mongoose.model(collection, Schema)
    }
}

module.exports.getModelWithSchema = (collection, schema) => {
    if (mongoose.models[collection]) {
        delete mongoose.models[collection];
    }
    const Schema = new mongoose.Schema(schema);
    return mongoose.model(collection, Schema)

}
