const { contextBridge, ipcRenderer } = require('electron')
// const CONSTANTS = require('./constant');
const APIS =[
    'connect',
    'disconnect',
    'getData',
    'getTotal',
    'getGroup',
    'getDistinctValue',
    'getFields',
    'getCollections',
    'favoriteConnections',
    'recentConnections',
    'getConnections',
    'addDashboard',
    'deleteDashboard',
    'editDashboard',
    'getDashboard'
  ];
  
const rendererObj = {
    ping: () => ipcRenderer.invoke('ping')
};

APIS.forEach(api => {
rendererObj[api] = (...args) => ipcRenderer.invoke(api, args)
})

contextBridge.exposeInMainWorld('API', rendererObj);